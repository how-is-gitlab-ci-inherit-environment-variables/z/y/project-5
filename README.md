# project-5
$MSG=

![Variables inside project](https://i.ibb.co/pdRCFjB/Screenshot-2020-03-05-at-12-45-42.png)

## [Gitlab CI Return](https://gitlab.com/how-is-gitlab-ci-inherit-environment-variables/z/y/project-5/pipelines/123502579)

- echo:
    ```bash
    $ echo $MSG
    Job succeeded
    ```

- echo with vars:
    ```bash
    $ echo $MSG
    Job succeeded
    ```
    
### Explain
The custom value in .gitlab-ci.yml doesn't work, because project's settings is the most important!